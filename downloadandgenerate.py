import urllib.request

from html_table_parser.parser import HTMLTableParser

import os

url = "https://forebears.io/brazil/surnames"

req = urllib.request.Request(
    url, 
    data=None, 
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
)
f = urllib.request.urlopen(req)

xhtml = f.read().decode('utf-8')

p = HTMLTableParser()

p.feed(xhtml)

final_list = p.tables[0][1:]

dir="wordlists"
os.mkdir(dir)

i = 0
j = 0
k = 0
for entry in final_list:
    j+=int(entry[2].replace(',',''))
    i+=1
    if ((j >      10**7 and k == 0) or
        (j >  5 * 10**7 and k == 1) or
        (j >  8 * 10**7 and k == 2) or
        (j > 10 * 10**7 and k == 3) or
        (j > 11 * 10**7 and k == 4) or
        (j > 12 * 10**7 and k == 5) or
        (j > 13 * 10**7 and k == 6) or
        (j > 14 * 10**7 and k == 7) or
        (j > 15 * 10**7 and k == 8)):
        with open(dir+"/"+str(k)+"-brazilian-surnames-"+str(j)+"citizens-"+str(i)+"surnames.txt",'w') as f:
            for line in final_list[:i]:
                f.write(line[1]+'\n')
        k+=1

with open(dir+"/"+str(k)+"-brazilian-surnames-"+str(j)+"citizens-"+str(i)+"surnames.txt",'w') as f:
            for line in final_list[:i]:
                f.write(line[1]+'\n')
