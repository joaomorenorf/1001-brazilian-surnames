# 1001 Brazilian Surnames

If you need a wordlist with 1001 Brazilian surnames, you came to the right place. You can access them directly through the wordlist folder. The data were scrapped from [Forebears.io](https://forebears.io/brazil/surnames).

## Getting started

This repository just holds the code I used to generate the wordlist and the wordlists can be found in wordlists folder.

## Running

```
python3 -m venv venv
source venv/bin/activate
pip3 install html-table-parser-python3
```

## License
The code is GPLv3
